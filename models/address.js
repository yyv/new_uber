const mongoose = require('mongoose');
const config = require('./../config/index.js');
const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;
mongoose.connect(config.db.url, {
  useMongoClient: true,
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries: 30
});

const AddressSchema = new Schema({
  start: {
    type: String,
    required: true
  },
  destination: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now,
  },
  response: {} //for response from UBER API
});

const AddresseModel = mongoose.model('Address', AddressSchema);
module.exports = AddresseModel;