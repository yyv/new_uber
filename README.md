
### What is this repository for? ###

this repository for test task 
https://docs.google.com/document/d/1FQJpo7ZgL0mliz3Mi7h_ODlYmHMKlUEIqGtgrT2VYvM/edit?ts=5a21620c

Review the application that you created:
If you had more time, what would you have changed in your code?

- Add common user part: registration, login, saved places and data by particular user and etc.
- save addresses not only as route "start/destination" but and as separate data. It can be useful for autocomplete
- add choosing addresses by google map
- localization support
- pagination for saved data in fron-tend
- more interaction with user. E.g. when user is waiting for response or when they are getting some error
- any template engine for view

What are the problems that you can foresee if the company needs to scale?
(Be also specific to changes you would have made in your code for scaling. You don't need to implement these changes.)

- For getting data from uber I'm  using 'node-uber' component it is a third-party software component.
So in this part there can be a problem with scaling. Because we don't know how it will work under high-load.
solution: use directly Uber API
- Another problem is that Uber API has rate limit. Probably, we may use several api keys as a solution.
- Use cache for route (start/destination) that was recently used.
- Use cache for getting saved data in DB
- for high-load we can use several node processes with load-balancer

And the last but the least
I used socket.io (just to try it). For this task this is unnecessary.
And a simple Ajax request will be enough for sending frontend request
