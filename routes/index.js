const routes = require('express').Router();
const views_path = './views/';

routes.get('/', function(req, res) {
  res.sendFile('index.html', {
    root: views_path
  });
});

routes.get('/history', function(req, res) {
  res.sendFile('history.html', {
    root: views_path
  });
});

routes.use("*", function(req, res) {
  res.sendFile("404.html", {
    root: views_path
  });
});

module.exports = routes;