const Uber = require('node-uber');
const config = require('./../config/index.js');
const Address = require('./../models/address.js');
const percent = config.percent;
const uber = new Uber(config.uber);

const getEstimates = function(addresses, socket) {
  uber.estimates.getPriceForRouteByAddressAsync(addresses.start, addresses.destination)
    .then(function(res) {
      let updatedData = changeGetingData(res);
      saveData(addresses, updatedData);
      socket.emit('getEstimates', {
        estimates: updatedData
      });
    })
    .error(function(err) {
      if (err.body !== undefined) {
        socket.emit('err', err.body.message);
      } else {
        socket.emit('err', String(err.cause));
      }
    });
}

const changeGetingData = function(data) {
  for (let i = 0; i < data.prices.length; i++) {
    let item = data.prices[i];
    data.prices[i]['new_high_estimate'] = item.high_estimate - (item.high_estimate * percent);
    data.prices[i]['new_low_estimate'] = item.low_estimate - (item.low_estimate * percent);
    data.prices[i]['new_estimate'] = item.currency_code + data.prices[i].new_low_estimate + '-' + data.prices[i].new_high_estimate;
    data.prices[i]['new_display_name'] = config.new_prefix + item.display_name[0].toUpperCase() + item.display_name.slice(1);
  };
  return data;
}

const saveData = function(addresses, data) {
  addresses.response = data;
  let new_addresses = new Address(addresses);
  new_addresses.save(function(err) {
    if (err) throw err;
    console.log('Addresses saved successfully!');
  });
}
module.exports.getEstimates = getEstimates;