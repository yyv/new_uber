const mongoose = require('mongoose');
const config = require('./../config/index.js');
const Address = require('./../models/address.js');
const getUberData = require('./../controllers/controller.js');

const ioEvents = function(io) {

  io.on('connection', function(socket) {
    console.log('user connected to socket');
    socket.on('disconnect', function() {
      console.log('user disconnected');
    });

    socket.on('send_adress', function(addresses) {
      if (addresses == undefined || addresses.destination == undefined || addresses.destination == '') {
        socket.emit('err', {
          estimates: 'err'
        });
        return false;
      }
      getUberData.getEstimates(addresses, socket);
    });

    socket.on('getHistory', function() {
      Address.find({}, function(err, data) {
        if (err) {
          socket.emit('err', err);
        }
        socket.emit('getHistory', data);
      });
      return false;
    });
  });
}

const init = function(app) {
  const server = app.listen(config.port, function() {
    console.log('app started and listening on port ' + config.port + '!');
  });
  const io = require('socket.io')(server);
  ioEvents(io);
  return server;
}

module.exports = init;