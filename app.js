const express = require('express');
const app = express();
const config = require('./config/index.js');
const routes = require('./routes');
const percent = config.percent;
const ioServer = require('./controllers/socket.js')(app);
app.use('/', routes);
ioServer.listen(config.port);